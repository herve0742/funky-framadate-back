# Funky Framadate Backend
Under heavy development...

## Get started
### Install
git clone && composer install
### Launch
docker compose up
### View api
visit localhost:8000/docs
### Run migration
docker exec -t funky_back_dev symfony console doctrine:migrations:migrate --no-interaction
### Load fixtures
docker exec -t funky_back_dev symfony console doctrine:fixtures:load --no-interaction
### Run tests
docker exec -t funky_back_dev sh docker/test/test.sh
