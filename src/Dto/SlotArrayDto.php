<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Slot;

class SlotArrayDto
{
    /**
     * Array of slots
     *
     * @var array<Slot>
     */
    #[Assert\Type('array')]
    #[Assert\All([
        new Assert\Type('App\Entity\Slot')
    ])]
    #[Assert\Valid]
    #[Groups(['post'])]
    public ?array $slots;
}
