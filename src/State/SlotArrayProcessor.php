<?php

namespace App\State;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\Persistence\ManagerRegistry;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Poll;
use App\Entity\Slot;

class SlotArrayProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly ManagerRegistry $registry
    ) {}

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $poll = $this->registry->getRepository(Poll::class)->findOneBy(['slug' => $uriVariables['slug']]);
        if ($poll == null) {
            throw new HttpException(404, 'Poll (' . $uriVariables['slug'] . ') not found.');
        }

        $manager = $this->registry->getManagerForClass(Slot::class);
        if ($manager == null) {
            throw new HttpException(500, 'Server Internal Error');
        }
        foreach ($data->slots as $slot) {
            $slot->setPoll($poll);
            $manager->persist($slot);
        }
        $manager->flush();
    }
}
