<?php

namespace App\State;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\Persistence\ManagerRegistry;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Poll;
use App\Entity\Slot;
use App\Entity\Participant;
use App\Entity\Vote;

class BallotProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly ManagerRegistry $registry
    ) {}

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $poll = $this->registry->getRepository(Poll::class)->findOneBy(['slug' => $uriVariables['slug']]);
        if ($poll == null) {
            throw new HttpException(404, 'Poll (' . $uriVariables['slug'] . ') not found.');
        }
        $participant = new Participant();
        $participant->setPseudo($data->pseudo);
        $poll->addParticipant($participant);
        foreach ($data->votes as $voteInput) {
            $vote = new Vote();
            $vote->setParticipant($participant);
            $vote->setValue($voteInput->value);
            $vote->setPoll($poll);
            $slot = $poll->getSlotById($voteInput->slotId);
            if ($slot == null) {
                throw new HttpException(422, 'Slot ID ' . $voteInput->slotId . ' is invalid.');
            }
            $vote->setSlot($slot);
            $poll->addVote($vote);
        }
        $manager = $this->registry->getManagerForClass(Poll::class);
        if ($manager == null) {
            throw new HttpException(500, 'Server Internal Error');
        }
        $manager->persist($poll);
        $manager->flush();
    }
}
