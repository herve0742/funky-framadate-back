<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Factory\PollFactory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        PollFactory::createPopulatedPoll(['slug' => 'fixture-1']);
        PollFactory::createPopulatedPoll(['slug' => 'fixture-2']);
        PollFactory::createPopulatedPoll(['slug' => 'fixture-3']);
        PollFactory::createPopulatedPoll(['slug' => 'fixture-4']);

        $manager->flush();
    }
}
