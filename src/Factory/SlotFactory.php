<?php

namespace App\Factory;

use App\Entity\Slot;
use App\Repository\SlotRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Slot>
 *
 * @method        Slot|Proxy create(array|callable $attributes = [])
 * @method static Slot|Proxy createOne(array $attributes = [])
 * @method static Slot|Proxy find(object|array|mixed $criteria)
 * @method static Slot|Proxy findOrCreate(array $attributes)
 * @method static Slot|Proxy first(string $sortedField = 'id')
 * @method static Slot|Proxy last(string $sortedField = 'id')
 * @method static Slot|Proxy random(array $attributes = [])
 * @method static Slot|Proxy randomOrCreate(array $attributes = [])
 * @method static SlotRepository|RepositoryProxy repository()
 * @method static Slot[]|Proxy[] all()
 * @method static Slot[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Slot[]|Proxy[] createSequence(iterable|callable $sequence)
 * @method static Slot[]|Proxy[] findBy(array $attributes)
 * @method static Slot[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Slot[]|Proxy[] randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<Slot> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<Slot> createOne(array $attributes = [])
 * @phpstan-method static Proxy<Slot> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<Slot> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<Slot> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<Slot> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<Slot> random(array $attributes = [])
 * @phpstan-method static Proxy<Slot> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<Slot> repository()
 * @phpstan-method static list<Proxy<Slot>> all()
 * @phpstan-method static list<Proxy<Slot>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<Slot>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<Slot>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<Slot>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<Slot>> randomSet(int $number, array $attributes = [])
 */
final class SlotFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'date' => self::faker()->dateTime(),
            'proposal' => self::faker()->text(25),
            'rank' => self::faker()->numberBetween(1, 127),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Slot $slot): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Slot::class;
    }
}
