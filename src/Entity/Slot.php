<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\SlotRepository;
use App\Repository\PollRepository;
use App\Dto\SlotArrayDto;
use App\State\SlotProcessor;

#[ORM\Entity(repositoryClass: SlotRepository::class)]
class Slot
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['getItem'])]
    private ?int $id = null;

    /** Poll */
    #[ORM\ManyToOne(inversedBy: 'slots')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Poll $poll = null;

    /**
     * Rank
     *
     * to order poll's slots
     */
    #[ORM\Column(type: Types::SMALLINT)]
    #[Assert\NotBlank]
    #[Assert\PositiveOrZero]
    #[Groups(['getItem', 'post'])]
    private ?int $rank = null;

    /**
     * Date proposal
     */
    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    // #[Assert\NotBlank] only if poll.kind === 'date'
    // #[Assert\When(
    //     expression: 'this.getPoll()->getType() === "date"',
    //     constraints: [
    //         new Assert\NotBlank(message: "In a date poll, proposal's date must be set.")
    //     ]
    // )]
    #[Assert\Type(\DateTime::class)]
    #[Groups(['getItem', 'post'])]
    private ?\DateTime $date = null;

    /**
     * Textual proposal
     */
    #[ORM\Column(length: 255, nullable: true)]
    // #[Assert\When(
    //     expression: 'this.getPoll()->getType() === "classic"',
    //     constraints: [
    //         new Assert\NotBlank(message: "In a classic poll, proposal's string must be set.")
    //     ]
    // )]
    #[Assert\Length(max: 255)]
    #[Groups(['getItem', 'post'])]
    private ?string $proposal = null;

    #[Assert\Callback]
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (
            $this->poll != null
            && $this->poll->getType() == 'date'
            && $this->date == null
        ) {
            $context->buildViolation('You must set a date')
                ->atPath('date')
                ->addViolation();
        }
        if (
            $this->poll != null
            && $this->poll->getType() == 'classic'
            && ($this->proposal == null || $this->proposal == "")
        ) {
            $context->buildViolation('You must set a proposal')
                ->atPath('proposal')
                ->addViolation();
        }
    }


    public ?string $slug;

    #[ORM\OneToMany(mappedBy: 'slot', targetEntity: Vote::class, orphanRemoval: true)]
    private Collection $votes;

    public function __construct()
    {
        $this->votes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoll(): ?Poll
    {
        return $this->poll;
    }

    public function setPoll(?Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getProposal(): ?string
    {
        return $this->proposal;
    }

    public function setProposal(?string $proposal): self
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * @return Collection<int, Vote>
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    public function addVote(Vote $vote): self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
            $vote->setSlot($this);
        }

        return $this;
    }

    public function removeVote(Vote $vote): self
    {
        if ($this->votes->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getSlot() === $this) {
                $vote->setSlot(null);
            }
        }

        return $this;
    }
}
